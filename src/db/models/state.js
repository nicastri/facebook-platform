import mongoose from 'mongoose';


const state = new mongoose.Schema({
    _id: String,
    state: {
        type: Array
    },
    fallback: {
        type: Object
    }
});

module.exports = state;