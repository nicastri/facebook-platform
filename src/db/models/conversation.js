import mongoose from 'mongoose';


const conversation = new mongoose.Schema({
    _id: String,
    appid: String,
    user: {
        type: Object
    },
    conversation: {
        type: Array
    },
    status: {
        type: Object
    }
});

module.exports = conversation;