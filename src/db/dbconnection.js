import mongoose from 'mongoose';


mongoose.connect('mongodb://localhost/local',{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  });

mongoose.connection.once("open", ()=>{
    console.log("Connessione avvenuta");
}).on("error", function(error){
    console.log("ERRORE!", console.log(error));
})

module.exports = mongoose;