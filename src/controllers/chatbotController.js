require("dotenv").config();

import request from "request";
import conversation from "../db/models/conversation";
import state from "../db/models/state";
import db from "../db/dbconnection";
import axios from "axios";
import conversation__ from "./conversationController";
import state__ from "./stateController";

const State = db.model('states', state);
const Conversazione = db.model('conversations', conversation); 
const MY_VERIFY_TOKEN = process.env.MY_VERIFY_TOKEN


//Chiamata per ottenere il webhook
let getWebhook = (req, res)=>{
    let VERIFY_TOKEN = MY_VERIFY_TOKEN
    
  // Parse the query params
  let mode = req.query['hub.mode'];
  let token = req.query['hub.verify_token'];
  let challenge = req.query['hub.challenge'];
    
  // Checks if a token and mode is in the query string of the request
  if (mode && token) {
  
    // Checks the mode and token sent is correct
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {
      
      // Responds with the challenge token from the request
      console.log('WEBHOOK_VERIFIED');
      res.status(200).send(challenge);
    
    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);      
    }
  }
}




//Incamero il postback di facebook!
let postWebhook =  (req, res)=>{
     // Parse the request body from the POST
    let body = req.body;

    console.log("-------------------------------------");
    // Checks this is an event from a page subscription
    if (body.object === 'page') {      
    // Iterates over each entry - there may be multiple if batched
    //console.log("Questo cosa è?", body.entry)
    body.entry.forEach(async function(entry) {

        // Gets the message. entry.messaging is an array, but 
        // will only ever contain one message, so we get index 0
        let webhook_event = entry.messaging[0];

        let recipient_id = entry.messaging[0].recipient.id;
        
        let sender_psid = webhook_event.sender.id;

        //console.log("----->", webhook_event);

        //Controllo di chi è il messaggio se del bot o dell'utente
        if(webhook_event.postback || !webhook_event.message.is_echo){
          console.log("::::::::::::::: UTENTE");
          let messaggio
          if(webhook_event.postback){
              messaggio = {
                domanda: webhook_event.postback.payload,
                time : webhook_event.timestamp
              }
          }else{
            messaggio = {
              domanda: webhook_event.message.text,
              time: webhook_event.timestamp
            }
          }
          
          let conv_ = await Conversazione.find({_id: sender_psid}, {appid :recipient_id});

          if(conv_.length === 0){
            console.log("<<Prima interazione>>");
            let user;
            try {
              user = await userData(sender_psid);
              let nuovo_utente = {
                nome: user.first_name,
                cognome: user.last_name
              }        
              

            let record = new Conversazione({_id: sender_psid, appid: recipient_id, user: nuovo_utente, conversation: [messaggio], status: {}});
            await record.save();
            } catch (error) {
              console.log("Big errore!")
            }

          }else{
            console.log("Qui stò pushando messaggi, nella conversazione", messaggio)
            await Conversazione.updateOne({_id:sender_psid }, {$push: {conversation: messaggio}});
          }

        }

        //LOGICA
        if (webhook_event.message) {
            console.log("SONO UN TEXT")
            handleMessage(sender_psid, webhook_event);
        } else if (webhook_event.postback) {
            console.log("SONO UN POSTBACK")
            handlePostback(sender_psid, webhook_event);
        }
    });

    // Returns a '200 OK' response to all requests
    res.status(200).send('EVENT_RECEIVED');
    } else {
    // Returns a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404);
    }
}

// Handles messages events
async function handleMessage(sender_psid, received_message) {    

  let response;  
  let messaggio_push; 

  console.log("sender_psid ", sender_psid, "received_message ",received_message)
  
  console.log("Funzione che mi restituisce il metch dello stato" );

  //faccio subito un check degli stati  
  if(!received_message.message.is_echo){
    
    console.log("message,,,,,,", received_message.message.text)
    
    //let checkStatus = async (state_id ,conversation_id,  input)
    let next = await state__.checkStatus(received_message.recipient.id ,sender_psid,  received_message.message.text);
    console.log("Next--------------->", next);
    console.log("Bellaaaaaaaaaaaaaaaaaaaaaa");

    if(next){
      console.log("abbiamo uno status non vuoto;")
      messaggio_push ={
        risposta: next.response.text,
        time : received_message.timestamp
      }
      if(next.fallback){
        messaggio_push.fallback = true;
      }

      console.log("?????????????????????????????????????messaggio_push", messaggio_push)

      await Conversazione.updateOne({_id:sender_psid }, {$push: {conversation: messaggio_push}});
      if(next.status){
        await Conversazione.updateOne({_id:sender_psid }, {$set: {status: next.status}});
      }else{
        await Conversazione.updateOne({_id:sender_psid }, {$set: {status: {}}});
      }
      
      response = next.response;

    }else{

      console.log("status vuoto");
      let stati = await state__.getAllStates(received_message.recipient.id);
      
      let match = state__.metchState(stati, received_message.message.text);

      console.log("match:::", match);
      messaggio_push ={
        risposta: match.response.text,
        time : received_message.timestamp
      }
    
      if(match.fallback){
        messaggio_push.fallback = true;
      }
      
      console.log("??", match.status||{});
      await Conversazione.updateOne({_id:sender_psid }, {$set: {status: match.status||{}}});

      await Conversazione.updateOne({_id:sender_psid }, {$push: {conversation: messaggio_push}});

      response = match.response;
    }

    callSendAPI(sender_psid, response); 

  }
}
    

// Handles messaging_postbacks events
async function handlePostback(sender_psid, received_postback) {
  console.log("sono un messaggio postback")
  let response;
  //Qui costruisco la funzione che ritorna il messaggio a seconda della pagina e ovviamente del postback

  let state = await state__.getAllStates(received_postback.recipient.id);

  console.log("Stati :::: ", state.states);

  state.states.forEach(async el=>{
    if(el.payload === received_postback.postback.payload){

      console.log("IOooooooooooooooooooooooo---->",el);
      let messaggio = {
          reisposta: el.response.text,
          time: received_postback.timestamp
      }
      response = el.response;
      await Conversazione.updateOne({_id:sender_psid }, {$push: {conversation: messaggio}});    
    }
  })

  console.log("tadan", response);

  callSendAPI(sender_psid, response); 
}



// Sends response messages via the Send API
function callSendAPI(sender_psid, response) {
  // Construct the message body
  let request_body = {
    "recipient": {
      "id": sender_psid
    },
    "message": response
  }

  request({
    "uri": "https://graph.facebook.com/v2.6/me/messages",
    "qs": { "access_token": process.env.PAGE_ACCESS_TOKEN },
    "method": "POST",
    "json": request_body
    }, (err, res, body) => {
        if (!err) {
        console.log('message sent!')
        } else {
        console.error("Unable to send message:" + err);
        }
    }); 
}


async function userData(sender_psid){
  try {
    const res = await axios.get("https://graph.facebook.com/"+ sender_psid+"?fields=first_name,last_name&access_token="+process.env.PAGE_ACCESS_TOKEN);
    console.log(res.data);
    console.log("credenziali ricevute");
    return res.data;
  } catch (error) {
    console.log("---------------------------------------------errore di axios");
    console.log(error)
  }
}

module.exports = {
    //test: test,
    getWebhook: getWebhook,
    postWebhook: postWebhook
}