import state from "../db/models/state";
import conversation from "../db/models/conversation";
import conversation__ from "./conversationController";

import db from "../db/dbconnection";

const State = db.model('states', state); 
const Conversation = db.model('conversations', conversation);


//Ritorna ritorna la collection di tutti gli stati del chatbot
let getAllStates = async (id)=>{
    let states = await State.find({_id: id});
    console.log("states->", states[0]._doc);
    return states[0]._doc;
}


////////////////////////////////////////////////////////////////////////////
//checkState: prende in ingresso tutti gli stati, e il tipo di attivatore
// 
//  l'activator è un oggetto che ci dice dove cercare 
//      {
//          activator: "string",
//          fallback: "true"||"false"    
//      }
////////////////////////////////////////////////////////////////////////////
let checkState = async ( id , activator, stati)=>{
    let states = stati
    if(!stati){
        console.log("Non mi sono arrivati gli stati quindi li cerco");
        states = await getAllStates(id);
    }    

    if(!activator.fallback){
        for(let i = 0; i<states.states.length; i++){
            if(states.states[i].payload===activator.activator){
                return states.states[i];
            }
        }
    }else{
        console.log("non ho trovato nulla negli stati da attivare allora cerco nei fallback");
        for(let i = 0; i< states.fallbacks.length; i++){
            if(states.fallbacks[i].payload===activator.activator){
                console.log("-----> trovato",  )
                return states.fallbacks[i];
            }
        }
    }
    console.log(">>>>>>>>>>>>>>>>>", states.fallback)
    return states.fallback;
}

//Questa funzione controlla lo status della conversazione:
// prende lo status della conversazione e il messaggio di ingresso
// si occupa del salvataggio dei campi e attiva lo stato da attivare 
// in base allo status della conversazione e l'input di ingresso

//ATTENZIONE: la funzione è responsabile del salvataggio dei campi
let checkStatus = async (state_id ,conversation_id,  input)=>{
    let conversazione =  await conversation__.getConversation(conversation_id);
    let activator = {}

    console.log("adesso---->",conversazione.status)
    let controlstatusempty = Object.keys(conversazione.status||{});
    console.log("controlstatusempty", controlstatusempty)
    if(controlstatusempty.length!==0){

        console.log("dio sto controllo non vale");

        let regex = new RegExp(conversazione.status.save.campo.validator);
        
        if(regex.exec(input)){
            
            console.log("espressione risolta")
            let user = conversazione.user

            user[conversazione.status.save.campo.name] = input;                                   

            await Conversation.updateOne({_id:conversation_id }, {$set: {user: user}});

            if(Array.isArray(conversazione.status.save.next)){
                activator.activator = conversazione.status.save.next.forEach(el=>{
                    if (el.activator === input){
                        return el.payload
                    }
                }) || input; 
            }else{                
                activator.activator = conversazione.status.save.next;
            }
            
            activator.fallback=  false

            return await checkState(state_id, activator);

        }else{
            console.log("espressione non risolta")
            if(conversazione.status.force){
                activator.activator = conversazione.status.save.fallback;
                activator.fallback = true;

                console.log("force a trueeeee");

                return await checkState(state_id, activator);
            }else{
                if(conversazione.status.nli){
                }else{
                    //Provo a metchare l'input con lo stato: se faccio il match lo ritorno altrimenti ritorno il fallback dello stato

                    console.log("controllino sullo status");
                    console.log("----->",conversazione.status);
                    
                    
                    let stati = await getAllStates(state_id);
                    activator.activator = input;
                    activator.fallback = false
                    let metch =  await checkState(state_id, activator, stati);
                    if(metch.fallback){
                        activator.fallback = true
                        activator.activator = conversazione.status.save.fallback
                        metch = await checkState(state_id, activator, stati);
                    }                              
                    
                    console.log("+++++++++++++++++++++++++++++++++++++", metch);
                    return metch;

                }
            }
        }
    }

    return false;
}


let metchState = (stati, payload)=>{
    console.log("cosa entra", payload);
    let t = undefined;
    stati.states.forEach(el=>{
        console.log("payoad", payload);
        console.log("el",el)
        if(el.payload ===payload){
            console.log("dio caneeee", el)
            t = el; 
        }
    });
    
    if (t){
        return t;
    }else{
        return stati.fallback;
    }
    
}



module.exports = {
    checkStatus: checkStatus,
    checkState: checkState,
    getAllStates: getAllStates,
    metchState: metchState
}