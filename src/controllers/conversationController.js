import conversation from "../db/models/conversation";
import db from "../db/dbconnection";

const Conversation = db.model('conversations', conversation); 


let getConversationStatus = async function(pid){
    let conv_ = await Conversation.find({_id:pid});
    return conv_[0].status
}

let setConversationStatus = async function(pid, status){
    await Conversation.updateOne({_id:pid }, {$set: {status: status}});
}

let getConversationUser = async function(pid){
    let conv_ = await Conversation.find({_id:pid});
    return conv_[0].user
}

let getConversation = async function(pid){
    let conv_ = await Conversation.find({_id:pid});
    return conv_[0];
}

module.exports = {
    getConversationStatus: getConversationStatus,
    setConversationStatus: setConversationStatus,
    getConversationUser : getConversationUser,
    getConversation: getConversation
}