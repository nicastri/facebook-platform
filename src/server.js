require("dotenv").config();
import express from "express";
import bodyParser from "body-parser";
import viewEngine from "./config/viewEngine.js";
import initWebRoutes from "./routes/web";


let app = express();

viewEngine(app);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//init web route
initWebRoutes(app);

let port = process.env.prot || 4010;

app.listen(port, ()=>{
    console.log("app listen on port " + port);
})
